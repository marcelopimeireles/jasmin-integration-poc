const express = require('express');

const SessionController = require('../src/app/controllers/SessionController');
const SalesController = require('../src/app/controllers/SalesController');
const PurchasesController = require('../src/app/controllers/PurchasesController');
const AccountsRecievableController = require('../src/app/controllers/AccountsRecievableController');
const MbwayController = require('../src/app/controllers/MbwayController');

const routes = express.Router();

// //Authentication
// routes.post('/', SessionController.getToken);

// //Create Sales Order
// routes.post('/sales/postOrders', SalesController.postOrders);
// routes.post('/sales/postOrder/:companyKey/:documentType/:serie/:seriesNumber', SalesController.postOrder);

// //Create Purchases Invoice Orders
// routes.post('/purchases/postOrderTax/:companyKey/:documentType/:serie/:seriesNumber', PurchasesController.postOrder);
// routes.post('/purchases/postOrderLine/:companyKey/:documentType/:serie/:seriesNumber', PurchasesController.postOrder);

// //Create Purchases Receipt
// routes.post('/purchases/postReceiptTax/:companyKey', AccountsRecievableController.postCreateReceipt);

// //Get Invoice List, Client List, Product List
// routes.get('/purchases/getInvoices', PurchasesController.get);
// routes.get('/purchases/getSupplierParties', PurchasesController.get);
// routes.get('/purchases/getPurchasesItems', PurchasesController.get);

routes.post('/operation', MbwayController.MerchantFinancialOperationWS);
routes.post('/operationInquiry', MbwayController.MerchantFinancialOperationInquiryWS);
routes.post('/createAlias', MbwayController.MerchantAliasWSCreate);
routes.post('/operational', MbwayController.MerchantOperationalWS);

routes.post('/FinancialOperationAsyncResult', MbwayController.FinancialOperationAsyncResult);
routes.post('/CreateMerchantAliasAsyncResult', MbwayController.CreateMerchantAliasAsyncResult);

module.exports = routes;
