const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const ObjectId = Schema.Types.ObjectId;

const financialOperationRequest = new Schema({
  operationTypeCode: {
    type: String,
    enum: ['022','023','024','025','026','048'],
    required: [true],
  },
  merchantOprId: {
    type: String,
    required: [true],
  },
  amount: {
    type: Number,
    required: [true],
  },
  currencyCode: {
    type: String,
    required: [true],
  },

});

module.exports = { financialOperationRequest };
