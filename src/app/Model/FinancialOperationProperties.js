const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const ObjectId = Schema.Types.ObjectId;

const financialOperationRequest = new Schema({
  purchaseToken: {
    type: String,
    required: [true],
  },
  initialTimestamp: {
    type: Date,
    required: [true],
  },
});

module.exports = { financialOperationRequest };
