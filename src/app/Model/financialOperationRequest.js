const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const ObjectId = Schema.Types.ObjectId;

const financialOperationRequest = new Schema({
  messageType: {
    type: String,
    required: [true],
  },
  aditionalData: {
    type: String,
    required: [false],
  },
  alias: {
    type: ObjectId,
    ref: 'Alias',
    required: [true],
  },
  financialOperation: {
    type: ObjectId,
    ref: 'FinancialOperation',
    required: [true],
  },
  referencedFinancialOperation: {
    type: ObjectId,
    ref: 'ReferencedFinancialOperation',
    required: [false],
  },
  merchant: {
    type: ObjectId,
    ref: 'Merchant',
    required: [true],
  },
  messageProperties: {
    type: ObjectId,
    ref: 'MessageProperties',
    required: [true],
  },
  financialOperationProperties: {
    type: ObjectId,
    ref: 'FinancialOperationProperties',
    required: [true],
  },
});

module.exports = { financialOperationRequest };
