const convert = require('xml-js');
const sampleHeaders = {'Content-Type': 'text/xml;charset=UTF-8'};
const options = {ignoreComment: true, alwaysChildren: true};
const dummyResponse = fs.readFileSync('../dummy/reqFinOpWS.xml', 'utf-8');

class Message {
  constructor (
    messageType,
    aditionalData = '',
  ) {
    this.arg0 = {
      messageType: messageType,
      aditionalData: aditionalData,
    }
  }

  setAlias (key, value) {
    this.arg0[key] = new Alias(value);
  }

  setFinOpr (key, value) {
    this.arg0[key] = new FinancialOperation(value);
  }

  setMerchant (key, value) {
    this.arg0[key] = new Merchant(value);
  }
}

class Alias {
  constructor(aliasName, aliasTypeCde) {
    this.aliasName = aliasName;
    this.aliasTypeCde = aliasTypeCde;
  }
};
class FinancialOperation {
  constructor(
    amount,
    currencyCode,
    operationTypeCode,
    merchantOprId,
    ) {
    this.amount = amount;
    this.currencyCode = currencyCode;
    this.operationTypeCode = operationTypeCode;
    this.merchantOprId = merchantOprId;
  }
};
class Merchant {
  constructor (iPAddress, posId) {
    this.iPAddress = iPAddress;
    this.posId = posId;
  }
};
// const MessageProperties {}; // App SDK only

const requestModel = {
  arg0: {
    messageType: "",
    aditionalData: "",
    alias: {
      aliasName: "351#960000000",
      aliasTypeCde: "001"
    },
    financialOperation: {
      amount: "",
      currencyCode: "",
      operationTypeCode: "",
      merchantOprId: "",
    },
    referencedFinancialOperation: {
      amount: "",
      currencyCode: "",
      operationTypeCode: "",
      merchantOprId: "",
    },
    merchant: {
      iPAddress: process.env.MERCHANT_IP_ADDRESS,
      posId: process.env.MERCHANT_POS_ID
    },
    messageProperties: {
      channel: process.env.MESSAGE_PROPERTIES_CHANNEL,
      apiVersion: process.env.MESSAGE_PROPERTIES_API_VERSION,
      channelTypeCode: process.env.MESSAGE_PROPERTIES_CHANNEL_TYPE_CODE,
      networkCode: process.env.MESSAGE_PROPERTIES_CHANNEL_NETWORK_CODE,
      serviceType: process.env.MESSAGE_PROPERTIES_SERVICE_TYPE,
      timestamp: process.env.MESSAGE_PROPERTIES_TIMESTAMP
    },
    newAlias: {
      aliasName: process.env.NEW_ALIAS_ALIAS_NAME,
      aliasTypeCde: process.env.NEW_ALIAS_ALIAS_TYPE_CDE
    }
  }
};

const xmlMessage = (hash, model) => {
`<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:fin="http://financial.services.merchant.channelmanagermsp.sibs/">
	<soapenv:Header xmlns:wsa="http://www.w3.org/2005/08/addressing">
		<wsa:Action>http://${MBWAY_DOMAIN}/${hash}</wsa:Action>
		<wsa:ReplyTo>
			<wsa:Address>http://${MBWAY_DOMAIN}/${hash}</wsa:Address>
			<wsa:ReferenceParameters/>
		</wsa:ReplyTo>
		<wsa:From>
			<wsa:Address/>
		</wsa:From>
		<wsa:MessageID/>
	</soapenv:Header>
    <soapenv:Body>
      <fin:${hash.split("/")[hash.split("/").lenght - 1]}>
        ${convert.js2xml(model, options)}
      </fin:${hash.split("/")[hash.split("/").lenght - 1]}>
    </soapenv:Body>
  </soapenv:Envelope>`;};

  model.exports = {
    sampleHeaders,
    xmlMessage,
    Message,
    Alias,
    FinancialOperation,
    Merchant
  }
