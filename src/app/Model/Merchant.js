const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const financialOperationRequest = new Schema({
  iPAddress: {
    type: String,
    required: [true],
  },
  posId: {
    type: String,
    required: [false],
  },
});

module.exports = { financialOperationRequest };
