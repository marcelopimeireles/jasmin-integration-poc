const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const financialOperationRequest = new Schema({
  channel: {
    type: String,
    enum: ['01','02','03','04','05'],
    required: [true],
  },
  apiVersion: {
    type: String,
    required: [false],
  },
  channelTypeCode: {
    type: String,
    required: [false],
  },
  networkCode: {
    type: String,
    required: [false],
  },
  serviceType: {
    type: String,
    required: [false],
  },
  timestamp: {
    type: Date,
    required: [false],
  },
});

module.exports = { financialOperationRequest };
