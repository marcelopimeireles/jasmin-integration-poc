const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const alias = new Schema({
  aliasName: {
    type: String,
    required: [true],
  },
  aliasTypeCde: {
    type: String,
    enum: ['001','002','010'],
    required: [false],
  },
});

module.exports = { alias };
