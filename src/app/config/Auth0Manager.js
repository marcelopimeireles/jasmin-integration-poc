const axios = require("axios");
const ManagementClient = require("auth0").ManagementClient;

module.exports = (function() {
  let managementClient;

  return {
    init,
    getClient,
    updateClient
  };

  function init() {
    return getToken()
      .then(data => data.access_token)
      .then(token => {
        const managementClient = new ManagementClient({
          domain: `${process.env.CLIENT_DOMAIN}`,
          token,
          scope: 'application',
        });

        // set it so we can use it in our other methods
        this.managementClient = managementClient;
        return true;
      })
      .catch(err => err);
  }

  function getToken() {
    // get the info we need
    const clientId = process.env.CLIENT_ID;
    const clientSecret = process.env.CLIENT_SECRET;
    const url = `https://${process.env.CLIENT_DOMAIN}/core/connect/token`;

    // make the call to the API via POST
    return axios
      .post(url, {
        client_id: clientId,
        client_secret: clientSecret,
        grant_type: "client_credentials",
        scope: 'application',
      })
      .then(res => res.data)
      .catch(err => err);
  }


  function getClient(clientId = null) {
    if (!clientId) clientId = process.env.CLIENT_ID;

    return this.managementClient
      .getClient({ client_id: clientId })
      .then(client => client)
      .catch(err => err);
  }

  function updateClient(data, clientId = null) {
    if (!clientId) clientId = process.env.CLIENT_ID;

    return this.managementClient
      .updateClient({ client_id: clientId }, data)
      .then(client => client)
      .catch(err => err);
  }
})();
