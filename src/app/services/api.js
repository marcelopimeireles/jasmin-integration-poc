const axios = require('axios');
require('dotenv').config();

const api = axios.create({
    baseURL: `https://${process.env.CLIENT_DOMAIN}`,
    headers: {
      'Authorization': `Bearer ${process.env.CLIENT_TOKEN}`,
    }
});

module.exports = api;
