const soapRequest = require('easy-soap-request');
const convert = require('xml-js');
require('dotenv').config();
const fs = require('fs');
const {
  sampleHeaders,
  xmlMessage,
  Message,
  FinancialOperation,
  } = require('../Model/financialOperationRequestModel');

module.exports = {
  // Lista de webservices disponibilizados ao Comerciante
  // Operações Financeiras (MerchantFinancialWSService)
  // MerchantFinancialOperationWS
  async MerchantFinancialOperationWS (req, res) {
    try {
      const finOpMessage = new Message ('N0003');
      finOpMessage.setMerchant('merchant', {
        iPAddress: process.env.MERCHANT_IP_ADDRESS,
        posId: process.env.MERCHANT_POS_ID
      });

      const { oprId, oprType, refOprId } = req.param;

      if (!oprId) {
        res.error(400).json({ error: 'Missing operation identifier.' });
      }

      const newFinancialOperation = new FinancialOperation ({
        operationTypeCode: oprType,
        merchantOprId: oprId,
        amount: '1520',
        currencyCode: '9782',
      });

      const refFinancialOperation = new FinancialOperation ({
        operationTypeCode: '',
        merchantOprId: refOprId,
        amount: '1520',
        currencyCode: '9782',
      });

      switch (operation) {
        case '022': // Compra
          const error = handleHasRef(refFinancialOperation, refOprId, res);
          if (error) return error;

          finOpMessage.setFinOpr('financialOperation', newFinancialOperation);
          break;

        case '023': // Devolução
          // const refFinancialOperation = FinancialOperation.findOne({
          //  merchantOprId: refOprId,
          // });
          refFinancialOperation.operationTypeCode = '022';

          const error = handleMissingRef(refFinancialOperation, '022', res);
          if (error) return error;

          finOpMessage.setFinOpr('financialOperation', newFinancialOperation),
          finOpMessage.setFinOpr('refFinancialOperation', refFinancialOperation);

          break;

        case '024': // Autorização de compra
          finOpMessage.setFinOpr('financialOperation', newFinancialOperation);
          break;

        case '025': // Compra após autorização
          // const refFinancialOperation = FinancialOperation.findOne({
          //  merchantOprId: refOprId,
          // });
          refFinancialOperation.operationTypeCode = '024';

          const error = handleMissingRef(refFinancialOperation, '024', res);
          if (error) return error;

          finOpMessage.setFinOpr('financialOperation', newFinancialOperation),
          finOpMessage.setFinOpr('refFinancialOperation', refFinancialOperation);

          break;

        case '026': // Cancelamento de autorização
          // const refFinancialOperation = FinancialOperation.findOne({
          //  merchantOprId: refOprId,
          // });
          refFinancialOperation.operationTypeCode = '024';

          const error = handleMissingRef(refFinancialOperation, '024', res);
          if (error) return error;

          finOpMessage.setFinOpr('financialOperation', newFinancialOperation),
          finOpMessage.setFinOpr('refFinancialOperation', refFinancialOperation);

          break;

        case '048': // Anulação
          // const refFinancialOperation = FinancialOperation.findOne({
          //  merchantOprId: refOprId,
          // });
          refFinancialOperation.operationTypeCode = '022';

          const error = handleMissingRef(refFinancialOperation, '022', res);
          if (error) return error;

          finOpMessage.setFinOpr('financialOperation', newFinancialOperation),
          finOpMessage.setFinOpr('refFinancialOperation', refFinancialOperation);

          break;

        default: // tertio non datur
          res.error(400).json({ error: 'Missing or invalid operation type.' });
          break;
      }

      const hash = '/Merchant/requestFinancialOperationWS'
      const url = `http://${process.env.MBWAY_DOMAIN}${hash}`;

      const { response } = await soapRequest(
        {
          url: url,
          headers: sampleHeaders,
          xml: xmlMessage(hash, requestModel),
          timeout: 300
        });

      return res.send(200).json(response.body);
    }
    catch (error) {
      console.log(
        error.response.config,
        error.isAxiosError
      );
    }
  },

  // MerchantFinancialOperationInquiryWS
  async MerchantFinancialOperationInquiryWS (req, res) {
    try {
      const hash = '/Merchant/financialOperationStatusInquiryWS'
      const url = `http://${process.env.MBWAY_DOMAIN}${hash}`;
      const { response } = await soapRequest(
        {
          url: url,
          headers: sampleHeaders,
          xml: xmlRequestModel(hash, requestModel),
          timeout: 300
        });

      return this.routeByStatus(response, res);
    }
    catch (error) {
      console.log(
        error.response.config,
        error.isAxiosError,
        ':: error'
      );
    }
  },

  // Operações de Alias (MerchantAliasWSService)
  // MerchantAliasWSCreate
  async MerchantAliasWSCreate (req, res) {
    try {




      const url = `http://${process.env.MBWAY_DOMAIN}/Merchant/createMerchantAliasWS`;
      const { response } = await soapRequest({
        url: url,
        headers: sampleHeaders,
        xml: xml,
        timeout: 300
      });

      const { headers, body, statusCode } = response;
      console.log(headers);
      console.log(body);
      console.log(statusCode);
    }
    catch (error) {
      console.log(
        error.response.config,
        error.isAxiosError,
        ':: error'
      );
    }
  },

  // MerchantAliasWSRemove
  async MerchantAliasWSRemove (req, res) {
    try {
      const request = {
        "arg0": {
          "messageType": process.env.MESSAGE_TYPE,
          "alias": {
            "aliasName": "351#960000000",
            "aliasTypeCde": "001"
          },
          "merchant": {
            "iPAddress": process.env.MERCHANT_IP_ADDRESS,
            "posId": process.env.MERCHANT_POS_ID
          },
          "messageProperties": {
            "channel": process.env.MESSAGE_PROPERTIES_CHANNEL,
            "apiVersion": process.env.MESSAGE_PROPERTIES_API_VERSION,
            "channelTypeCode": process.env.MESSAGE_PROPERTIES_CHANNEL_TYPE_CODE,
            "networkCode": process.env.MESSAGE_PROPERTIES_CHANNEL_NETWORK_CODE,
            "serviceType": process.env.MESSAGE_PROPERTIES_SERVICE_TYPE,
            "timestamp": process.env.MESSAGE_PROPERTIES_TIMESTAMP
          },
          "newAlias": {
            "aliasName": process.env.NEW_ALIAS_ALIAS_NAME,
            "aliasTypeCde": process.env.NEW_ALIAS_ALIAS_TYPE_CDE
          }
        }
      };

      const xml = `<soapenv:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">
                   <soapenv:Header/>
                    <soapenv:Body>
                      ${convert.js2xml(request, options)}
                    </soapenv:Body>
                  </soapenv:Envelope>`;

      const url = `http://${process.env.MBWAY_DOMAIN}/Merchant/createMerchantAliasWS`;
      const { response } = await soapRequest({
        url: url,
        headers: sampleHeaders,
        xml: xml,
        timeout: 300
      });

      const { headers, body, statusCode } = response;
      console.log(headers);
      console.log(body);
      console.log(statusCode);
    }
    catch (error) {
      console.log(
        error.response.config,
        error.isAxiosError,
        ':: error'
      );
    }
  },

  // Serviços Operacionais (MerchantOperationalWSService)
  // MerchantOperationalWS
  async MerchantOperationalWS (req, res) {},

  // Lista de webservices disponibilizados pelo Comerciante
  // FinancialOperationAsyncResult
  async FinancialOperationAsyncResult (req, res) {},

  // CreateMerchantAliasAsyncResult
  async CreateMerchantAliasAsyncResult (req, res) {
    requestModel.arg0.messageType = 'N0003';

  },

  handleMissingRef(refFinOp, opTypeCode, res) {
    if (refFinOp && refFinOp.operationTypeCode  !== opTypeCode) {
      return res.error(400).json({ error: 'Missing referral operation.' });
    }
  },

  handleHasRef(refFinOp, res) {
    const values = Object.values(refFinOp);
    if (!values.every((value)=> !value) && refOprId) {
      return res.error(400).json({ error: 'This operation can´t refeer to other.' });
    }
  }
}
