const fs = require('fs');
const axios = require('axios');
const oauth = require('axios-oauth-client');
const envfile = require('envfile');
require('dotenv').config();
const sourcePath = '.env';

module.exports = {
  async getToken (req, res) {
    try {
      const credentials = {
        url: `https://${process.env.CLIENT_TOKEN_DOMAIN}/core/connect/token`,
        grant_type: 'client_credentials',
        client_id: process.env.CLIENT_ID,
        client_secret: process.env.CLIENT_SECRET,
        scope: 'application'
      };

      const token = await oauth.client(axios.create(), credentials)();

      let parsedFile = envfile.parseFileSync(sourcePath);
      parsedFile.CLIENT_TOKEN = token.access_token;
      fs.writeFileSync('./.env', envfile.stringifySync(parsedFile))

      return res.json(true);
    }
    catch (error) {
      console.log(
        error.response.config,
        error.isAxiosError,
        ':: error'
      );
    }
  }
}
