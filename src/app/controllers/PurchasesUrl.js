require('dotenv').config();

module.exports = {
  baseUrl: `/api/${process.env.CLIENT_ACCOUNT}/${process.env.CLIENT_SUBSCRIPTION}`,
  getOrdersUrl: '/purchases/orders',
  getInvoicesUrl: '/invoiceReceipt/invoices',
  getPurchasesItemsUrl: '/purchasesCore/purchasesItems/extension',
  getSupplierPartiesUrl: '/purchasesCore/supplierParties/extension',

  postOrdersUrl: '/purchases/orders',
  postOrderTaxUrl: 'documentTaxes',
  postOrderLineUrl: 'documentLines',

  postInvoicesUrl: '/invoiceReceipt/invoices',
  postOrderTaxUrl: 'documentTaxes',
  postOrderLineUrl: 'documentLines',
}
