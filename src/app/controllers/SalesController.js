const api = require('../services/api');
const salesUrl = require('./SalesUrl');

module.exports = {
  async postOrders (req, res) {
    try {
      const request = req.originalUrl.split('/');
      const endpoint = `${request[request.length-1]}Url`;
      const url = salesUrl[endpoint];
      console.log(salesUrl.baseUrl + url);
      const response = await api.post(salesUrl.baseUrl + url);
      return res.json(response.data);
    }
    catch (error) {
      console.log(
        error.response.config,
        error.isAxiosError,
        ':: error'
      );
    }
  },

  async postOrder (req, res) {
    try {
      const {originalUrl, companyKey, documentType, serie, seriesNumber} = req;
      const reqVars = [companyKey, documentType, serie, seriesNumber].join('/');
      const request = originalUrl.split('/');
      const endpoint = `${request[2]}sUrl`;
      const url = `${salesUrl[endpoint]}${reqVars}/${salesUrl.postOrderLineUrl}`;
      console.log(salesUrl.baseUrl + url);
      const response = await api.post(salesUrl.baseUrl + url);
      return res.json(response.data);
    }
    catch (error) {
      console.log(
        error.response.config,
        error.isAxiosError,
        ':: error'
      );
    }
  }
}
