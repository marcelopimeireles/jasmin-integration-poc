require('dotenv').config();

module.exports = {
  baseUrl: `/api/${process.env.CLIENT_ACCOUNT}/${process.env.CLIENT_SUBSCRIPTION}`,
  postOrdersUrl: '/sales/orders',
  postOrderLineUrl: 'documentLines',
}
