const api = require('../services/api');
const AccountsRecievableUrl = require('./AccountsRecievableUrl');
require('dotenv').config();

module.exports = {
  async postCreateReceipt (req, res) {
    try {
      const companyKey = !(req.companyKey) ? process.env.COMPANY_KEY: req.companyKey;
      const endpoint = req.originalUrl.split('/')[2];
      const url = `${endpoint}/${companyKey}`;
      console.log(AccountsRecievableUrl.baseUrl + url);
      const response = await api.post(AccountsRecievableUrl.baseUrl + url);
      console.log('response: ', response.header);

      return res.json(response.data);
    }
    catch (error) {
      console.log(
        error.response.config,
        error.isAxiosError,
        ':: error'
      );
    }
  }
}
