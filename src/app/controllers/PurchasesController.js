const api = require('../services/api');
const purchasesUrl = require('./PurchasesUrl');

module.exports = {
  async get (req, res) {
    try {
      const request = req.originalUrl.split('/');
      const endpoint = `${request[request.length-1]}Url`;
      const url = purchasesUrl[endpoint];
      console.log(purchasesUrl.baseUrl + url);
      const response = await api.get(purchasesUrl.baseUrl + url);
      return res.json(response.data);
    }
    catch (error) {
      console.log(
        error.response.config,
        error.isAxiosError,
        ':: error'
      );
    }
  },

  async post (req, res) {
    try {
      const request = req.originalUrl.split('/');
      const endpoint = `${request[request.length-1]}Url`;
      const url = purchasesUrl[endpoint];
      console.log(purchasesUrl.baseUrl + url);
      const response = await api.post(purchasesUrl.baseUrl + url);
      return res.json(response.data);
    }
    catch (error) {
      console.log(
        error.response.config,
        error.isAxiosError,
        ':: error'
      );
    }
  },

  async postOrder (req, res) {
    try {
      const endpoint = purchasesUrl.postOrdersUrl;
      const {companyKey, documentType, serie, seriesNumber} = req.params;
      const reqVars = [companyKey, documentType, serie, seriesNumber].join('/');
      const request = req.originalUrl.split('/');
      const endpointFinal = `${request[2]}Url`;
      const url = `${endpoint}/${reqVars}/${purchasesUrl[endpointFinal]}`;
      console.log('url :::: ', purchasesUrl.baseUrl + url);
      const response = await api.post(purchasesUrl.baseUrl + url);
      return res.json(response.data);
    }
    catch (error) {
      console.log(
        error.response.status,
        error.response.statusText,
        ', isAxiosError: ',
        error.isAxiosError
      );
    }
  }
}
