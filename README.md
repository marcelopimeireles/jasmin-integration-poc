# Jasmin-Integration-POC
Proof of concept of an API calling Jasmin Software API.

## .ENVFILE
#### Permanent variables
* CLIENT_TOKEN_DOMAIN
* CLIENT_DOMAIN

#### Application variables (for each new Jasmin Account)
* CLIENT_ID
* CLIENT_SECRET

#### Generated values (may be stored here)
* CLIENT_ACCOUNT
* CLIENT_SUBSCRIPTION
* CLIENT_TOKEN

#### Dummy variables (POC adhoc)
* COMPANY_KEY
* DOCUMENT_TYPE
* SERIE
* SERIES

---

Routes implemented:

## Authentication
* '/' - Get a Token (with id and password in .envfile)

## Sales
#### Orders
* '/sales/postOrders' - Creates a new entity record in Jasmin.
* '/sales/postOrder/:companyKey/:documentType/:serie/:seriesNumber' - Inserts a new 'Order Line' in the 'Order'.

## Purchases
#### Invoice Orders
* '/purchases/postOrderTax/:companyKey/:documentType/:serie/:seriesNumber'  - Inserts a new 'Invoice Tax' in the 'Invoice'.
* '/purchases/postOrderLine/:companyKey/:documentType/:serie/:seriesNumber' - Inserts a new 'Invoice Line' in the 'Invoice'.
#### Create Purchases Receipt List
* /purchases/postReceiptTax/:companyKey' - Returns the list of all the entity records available.
#### Get Invoice List
* '/purchases/getInvoices' - Returns the list of all the entity records available.
#### Get Client List
* '/purchases/getSupplierParties' - Returns the list of all the entity records available.
#### Get Product List
* '/purchases/getPurchasesItems' - Returns the list of all the entity records available.

---

## Sales Order Path:
#### 1. Build an ApiOrderDocumentLinesResource Object:
You can get the data by retrieving Sales>getOrders (all records) and picking the values of a same object of the collection since its DocumentLineStatus is 1 (Open).

###### DocumentLineStatus
1. Open
2. Completed

#### 2. Insert a new Order Line in Order and parte the ApiOrderDocumentLinesResource as a value thought endpoit: '/sales/postOrder/:companyKey/:documentType/:serie/:seriesNumber'

https://jasminsoftware.github.io/sales.orders.html#ApiOrderDocumentLinesResource
```javascript
{
   description (string),
   quantity (decimal),
   unitPrice (decimal),
   deliveryDate (Datetime),
   unit (string),
   salesItem (string),
   itemTaxSchema (Datetime),
   documentLineStatus (enum DocumentLineStatus),
}
```
###### DocumentLineStatus
1. Open
2. Completed

#### 3. Instructs the 'Invoices' service to process the specified entity records.

#### 4. It must call an 'Invoice by key', and return an InvoiceResouce Object:
https://jasminsoftware.github.io/billing.invoices.html#InvoiceResource
```javascript
{
   documentType (string),
   serie (string),
   seriesNumber (int),
   company (string),
   paymentMethod (string),
   currency (string),
   documentDate (Datetime),
   postingDate (Datetime),
   buyerCustomerParty (string),
   buyerCustomerPartyName (string),
   accountingParty (string),
   exchangeRate (decimal),
   discount (decimal),
   cashInvoice (bool),
   grossValue (decimal),
   allowanceChargeAmount (decimal)
}
```
